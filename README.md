# Developer Documentation and Environment Setup
This repository holds the [ADRs](https://github.com/joelparkerhenderson/architecture-decision-record#what-is-an-architecture-decision-record) for the MoonCat​Rescue project, as well as scripts and tools to help implement those working standards. Browse through the documents in the `./adr` folder to learn about coding standards for these projects.

In general, projects within the MoonCat​Rescue ecosystem use [Docker](https://www.docker.com/) as an infrastructure base ([ADR0002](./adr/0002-docker-development-environments.md)), and uses NodeJS (Javascript) containers to develop in. For solidity projects, the [Hardhat](https://hardhat.org/) library is used. For web UI projects, [Next.js](https://nextjs.org/) is preferred for overall site scaffolding, with [Ethers](https://ethers.org/) or [viem](https://viem.sh/), [Web3Modal](https://web3modal.com/), and [wagmi](https://wagmi.sh/) libraries to interface with the Ethereum network.

## To get started developing on a project within the MoonCat​Rescue ecosystem:

- Install [Docker](https://www.docker.com/)
- Run `build.sh` to create the `node:mooncat` and `eth-security-toolbox:mooncat` images locally

Then, you can check out any other repository in this group, and run `docker-compose up` to get a development environment running. For more details, see [ADR0002](./adr/0002-docker-development-environments.md), or the `README` of the individual project repositories.

# NodeJS
The `Dockerfile.node` file in this repository adds customization on top of the `node:22` base image. Namely, it configures the container to use the `node` user (instead of `root`; recommended by that project as a best practice), and sets that `node` user’s UID/GID to a known value (25600) for then being able to be relied-upon in other services. For more details on how NodeJS interacts in a Docker environment, see [ADR0002](./adr/0002-docker-development-environments.md#nodejs).

If you have set up a `mooncat` user on your local workstation with the UID of 25600, you can use the `permissions.sh` script to give your checked-out code folders the proper permissions to be able to run well in these Docker containers.

The `Dockerfile.node` file also enables Firebase development and local emulation (includes Java and a global install of `firebase-tools`).

Running the `node.sh` will bring up a Node environment in the current folder.

## Node package configuration
Here's a basic `package.json` template for bootstrapping MoonCat-related Solidity projects:

```json
{
  "name": "mooncat-project",
  "version": "1.0.0",
  "license": "AGPL-3.0",
  "devDependencies": {
    "@nomiclabs/hardhat-ethers": "^2.0.2",
    "@nomiclabs/hardhat-etherscan": "^3.1.0",
    "@nomiclabs/hardhat-waffle": "^2.0.1",
    "chai": "^4.3.4",
    "dotenv": "^8.2.0",
    "ethereum-waffle": "^3.3.0",
    "ethers": "^5.3.0",
    "hardhat": "^2.9.3",
    "prettier": "^2.7.1"
  },
  "prettier": {
    "quoteProps": "preserve",
    "semi": false,
    "singleQuote": true
  }
}
```

And for bootstrapping MoonCat-related web applications for interacting with Web3 data:

```json
{
  "name": "mooncat-project",
  "version": "1.0.0",
  "license": "AGPL-3.0",
  "dependencies": {
    "@web3modal/ethereum": "^2.0.0",
    "@web3modal/react": "^2.0.0",
    "ethers": "^5.7.0",
    "next": "12.3.4",
    "react": "18.2.0",
    "react-dom": "18.2.0",
    "wagmi": "^0.11.2"
  },
  "devDependencies": {
    "@types/node": "18.8.2",
    "@types/react": "18.0.21",
    "@types/react-dom": "18.0.6",
    "eslint": "8.33.0",
    "eslint-config-next": "12.3.4",
    "prettier": "^2.7.1",
    "sass": "^1.55.0",
    "typescript": "^4.9.5"
  },
  "prettier": {
    "quoteProps": "preserve",
    "semi": false,
    "singleQuote": true
  }
}
```

# Hardhat Node
The Hardhat toolset has the ability to run as [a standalone Ethereum node](https://hardhat.org/hardhat-network/docs/overview), forking mainnet at a specific block height and thereafter running as a private side-chain. It then exposes port 8545 as an RPC endpoint for interacting with that node.

The `hardhat-node.sh` script is a convenience script for starting up a standalone Hardhat node, using the MoonCat​Rescue NodeJS Docker setup, and exposing the appropriate ports.

# ETH Security Toolbox environment
The Trail of Bits team has put together an [Ethereum Security Toolbox](https://github.com/trailofbits/eth-security-toolbox) that is a Docker image with several smart contract analysis tools. In order to work with the local user setup the MoonCat​Rescue team uses (see [ADR0002](./adr/0002-docker-development-environments.md)), the `ethsec` user the Trail of Bits team uses in their image needs to be switched over to UID 25600. To make a local image that has that, run:

```sh
docker pull trailofbits/eth-security-toolbox
docker build -t eth-security-toolbox:mooncat -f Dockerfile.security-toolbox .
```
