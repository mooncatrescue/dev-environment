#!/usr/bin/env bash

docker run -it --rm -p 8545:8545 -u node -v ${PWD}:/app -w /app node:mooncat npx hardhat node
