# Proposal to Create Draft ADR
This Merge Request is a request to add a new Architecture Decision Record (ADR) to the repository. New ADRs get added in a “proposed” state, and so discussions in this Merge Request should not focus on whether or not the ADR should be accepted, but should rather focus on if the drafted ADR is clear, well-formatted and complete.

# Feedback
If you agree with this draft being promoted to a “proposed” ADR, either leave a comment, or use the emoji response on this merge request.

If you have feedback on this ADR draft, please be as specific as possible. All feedback should be respectful and constructive.

Possible questions to consider as you’re reviewing this draft ADR:

- Is the “Context” section unambiguous and truthful? It should be clear enough that when developers move to implement it, it answers the “why...?” questions they may have
- Is the “Decision” section actionable? It should be clear enough that when developers move to implement it, it answers the “how...?” questions they may have.
- Does the “Consequences” section have a balance between both positive and negative results from this change?

The project maintainers generally review code on a weekly basis, so feedback will be given generally within 7-10 business days. Draft ADRs that dont receive objections are generally approved to be merged in.

Thank you for taking the time to review this draft ADR and provide feedback. Your input is greatly appreciated.