# Proposal to Modify an ADR
This Merge Request is a request to change an existing Architecture Decision Record (ADR) that has already been accepted.

<!-- Summarize here the key changes made to this ADR. If part of this change is to move the ADR to a different status, explain that here. -->

# Feedback
If you agree with this ADR being updated in the manner described in this merge request, either leave a comment, or use the emoji response on this merge request.

If you have feedback to give on this ADR proposal, please be as specific as possible. All feedback should be respectful and constructive.

Possible questions to consider as you’re reviewing this ADR:

- Is the “Decision” a good one? Does it solve the problem(s) outlined in the “Context” while avoiding too many negative “Consequences”?
- Was enough research done on alternatives? Is it clear why the proposed “Decision” is the best one, or is more context needed to illustrate why the alternatives are not as desirable?

The project maintainers generally review code on a weekly basis, so feedback will be given generally within 7-10 business days.

Thank you for taking the time to review this proposed ADR and provide feedback. Your input is greatly appreciated.