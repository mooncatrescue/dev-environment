# Proposal to Transition ADR to Accepted State
This Merge Request is a request to transition an existing Architecture Decision Record (ADR) from the “proposed” state to “accepted”, meaning it’s then an active part of the project’s policy.

# Changes from Proposed
In the course of discussions moving from the initial proposal, here are the things that have changed:

<!-- Create bulleted list summarizing changes made as discussions in the Merge Request happen -->
N/A

# Feedback
If you agree with this ADR being promoted to an approved ADR, either leave a comment, or use the emoji response on this merge request.

If you have feedback to give on this ADR proposal, please be as specific as possible. All feedback should be respectful and constructive.

Possible questions to consider as you’re reviewing this ADR:

- Is the “Decision” a good one? Does it solve the problem(s) outlined in the “Context” while avoiding too many negative “Consequences”?
- Was enough research done on alternatives? Is it clear why the proposed “Decision” is the best one, or is more context needed to illustrate why the alternatives are not as desirable?

The project maintainers generally review code on a weekly basis, so feedback will be given generally within 7-10 business days.

Thank you for taking the time to review this proposed ADR and provide feedback. Your input is greatly appreciated.