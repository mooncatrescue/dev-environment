# Proposal to Create an ADR
This project uses Architecture Decision Records (ADRs) to record structural decisions and organizing principles (more details at [ADR0001](/adr/0001-record-architecture-decisions.md)). This Issue is an idea to create a new ADR for the project. Discussion on this issue should be about whether the topic should be made into an ADR.

# Proposal
<!-- Summarize the need for an ADR here. This information will likely fit well into the “Context” section of the draft ADR. Illustrate what is the issue we’re seeing that is motivating the need for a decision or change. -->

# Feedback
If you agree with this draft being promoted to a “proposed” ADR, either leave a comment, or use the emoji response on this issue.

If you have feedback on this ADR proposal, please be as specific as possible. All feedback should be respectful and constructive.

Possible questions to consider as you’re reviewing this ADR proposal:

- Is the problem outlined a big-enough problem (or one with multiple possible solutions) that it merits an ADR?
- Is it confusing to new developers and would slow down their onboarding process if this was not described in an ADR?

The project maintainers generally review code on a weekly basis, so feedback will be given generally within 7-10 business days.

Thank you for taking the time to review this proposed ADR and provide feedback. Your input is greatly appreciated.