#!/usr/bin/env bash

RESET='\033[0m'
GREEN='\033[1;32m'

echo ""
echo -e "${GREEN}Building Node.js container..."
echo -e "===============================${RESET}"
docker pull node:22
docker build --no-cache -t node:mooncat -f Dockerfile.node .

echo ""
echo -e "${GREEN}Building ETH Security Toolbox container..."
echo -e "============================================${RESET}"
docker pull trailofbits/eth-security-toolbox
docker build --no-cache -t eth-security-toolbox:mooncat -f Dockerfile.security-toolbox .

echo ""
echo -e "${GREEN}Done!${RESET}"
