# 0001 - Record Architecture Decisions
**Updated:** 5 Mar 2023

## Status
<!-- Proposed | Accepted | Rejected | Deprecated | Superseded -->
Accepted

## Context
<!-- What is the issue that we’re seeing, that is motivating this decision or change -->
We need to record the architecture deciions made for this team and company.

## Decision
<!-- What is the change that we’re actually proposing or doing. -->
We will use Architecture Decision Records, as described by [this repository](https://github.com/joelparkerhenderson/architecture-decision-record).

ADRs will be written in Markdown and stored in the `dev-environment` repository (version-controlled by Git, stored in GitLab). They will be rendered into HTML on the GitLab site for browsing and review.

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in any ADR document in this repository are to be interpreted as described in [RFC 2119](https://www.rfc-editor.org/rfc/rfc2119).

## Consequences
<!-- Outcomes, both positive and negative -->
See blog article linked under “Decision”.

To create a new ADR, create a new file in this folder, copying the `0000-TEMPLATE.md` file for structure.
